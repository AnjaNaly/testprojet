/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objet;

import annotations.Chemin;
import annotations.Colonne;
import annotations.Tables;
import annotations.Seq;
import fonction.Fonction;
import fonction.Requete;
import java.util.HashMap;
import util.ModelView;

/**
 *
 * @author anjar
 */
@Tables(nom = "Departement")
@Seq(nom = "s_dept")
@Chemin(url = "Dept")
public class Dept {

    @Colonne(col = "iddept")
    String id;
    @Colonne(col = "nomdept")
    String nom;

    public Dept() {
    }

    public Dept(String nom) {
        this.nom = nom;
    }

    public Dept(String id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    //eto no anaovana ny service en qlq sorte 
    public ModelView add() {
        return new ModelView();
    }

    @Chemin(url = "add")
    public ModelView create() throws Exception {
        ModelView mv = new ModelView();
        if (this.id != null && this.nom != null) {
            System.out.println(" id : "+this.id);
            System.out.println("nom : "+this.nom);
            new Fonction().insertobj(this);
            System.out.println("sql : "+new Requete().create(this));
        }
        mv.setUrl("ajoutdept.jsp");
        return mv;
    }

    @Chemin(url = "list")
    public ModelView read() throws Exception {
        Object[] obj = new Fonction().selectobj(this);
        ModelView mv = new ModelView();
        mv.setUrl("listedept.jsp");
        HashMap<String, Object[]> hm = new HashMap<String, Object[]>();
        hm.put("listedept", obj);
        mv.setLister(hm);
        return mv;
    }
}
